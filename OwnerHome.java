package cus1166group2classproject;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
public class OwnerHome {
  public static void main(String[] args) {
    OwnerHome ownerHome = new OwnerHome();
    ownerHome.show();
  }

  private JFrame frame;
  private Database db;

  public OwnerHome() {
    frame = new JFrame("Owner Home");
    db = new Database();
    createWindow();
    createGUI();
  }

  public void show() {
    frame.setVisible(true);
  }

  private void createWindow() {
    frame.setSize(300, 200);
    frame.setLocationRelativeTo(null);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  }

  private void createGUI() {
    // Full panel
    JPanel panel = new JPanel();
    panel.setLayout(new FlowLayout());

    // Panel for login button
    JPanel loginPanel = new JPanel();

    // Owner ID Input Panel
    JPanel ownerIdPanel = new JPanel();
    ownerIdPanel.setLayout(new FlowLayout());
    JLabel ownerIdLabel = new JLabel("Owner ID");
    JTextField ownerIdTxt = new JTextField(12);
    ownerIdPanel.add(ownerIdLabel);
    ownerIdPanel.add(ownerIdTxt);
    loginPanel.add(ownerIdPanel);

    JButton loginBtn = new JButton("Login");
    loginBtn.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
          if (validOwnerId(ownerIdTxt.getText())) {
            new OwnerDashboard().show();
            frame.dispose();
          } else {
            JOptionPane.showMessageDialog(frame, "Invalid Owner ID.");
          }
      }
    });
    loginPanel.add(loginBtn);

    // Panel for registration button
    JPanel registrationPanel = new JPanel();
    JButton registerBtn = new JButton("Register");
    registerBtn.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
          new VehicleRegistrationFrame().show();
      }
    });
    registrationPanel.add(registerBtn);

    loginPanel.setBorder(BorderFactory.createTitledBorder("Dashboard Login"));
    loginPanel.setPreferredSize(new Dimension(250, 100));
    registrationPanel.setBorder(BorderFactory.createTitledBorder("Vehicle Registration"));
    registrationPanel.setPreferredSize(new Dimension(250, 60));
    panel.add(loginPanel);
    panel.add(registrationPanel);

    frame.add(panel);
  }

  private boolean validOwnerId(String id) {
    String[] vehicles = db.getVehicleData();

    for (String vehicle : vehicles) {
      String[] vehicleFields = vehicle.split("-");
      if (id.equals(vehicleFields[1])) return true;
    }
    return false;
  }
}
