package cus1166group2classproject;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;

public class Database {
  public static void main(String[] args) {
    Database db = new Database();

    for (String vehicle : db.getVehicleData()) System.out.println(vehicle);
  }
  File vehicleData;

  public Database() {
    try {
      vehicleData = new File("vehicle_data.txt");
    } catch (Exception ex) {
      System.out.println("Vehicle data file not found.");
    }
  }

  public String[] getVehicleData() {
    Scanner scan = null;
    List<String> vehicleList = new ArrayList<>();
    try {
      scan = new Scanner(vehicleData);
      while (scan.hasNextLine()) vehicleList.add(scan.nextLine());
    } catch (FileNotFoundException ex) {
      System.out.println("Cannot read vehicle data file.");
    }
    return vehicleList.toArray(new String[vehicleList.size()]);
  }
}
