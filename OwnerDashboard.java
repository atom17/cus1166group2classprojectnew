package cus1166group2classproject;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class OwnerDashboard {
  public static void main(String[] args) {
    OwnerDashboard ownerDashboard = new OwnerDashboard();
    ownerDashboard.show();
  }

  private JFrame frame;

  public OwnerDashboard() {
    frame = new JFrame("Owner Dashboard");
    createWindow();
    createGUI();
  }

  public void show() {
    frame.setVisible(true);
  }

  private void createWindow() {
    frame.setSize(400, 400);
    frame.setLocationRelativeTo(null);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  }

  private void createGUI() {
    // Full panel
    JPanel panel = new JPanel();
    panel.setLayout(new FlowLayout());

    // Owner dashboard header Panel
    JPanel headerPanel = new JPanel();
    headerPanel.setLayout(new BorderLayout());
    headerPanel.setPreferredSize(new Dimension(350, 30));

    JLabel headerLabel = new JLabel("Owner Dashboard");
    JButton logoutBtn = new JButton("logout");
    headerPanel.add(headerLabel, BorderLayout.WEST);
    headerPanel.add(logoutBtn, BorderLayout.EAST);
    panel.add(headerPanel);

    // Vehicel Name Input Panel
    // JPanel vehicleNamePanel = new JPanel();
    // vehicleNamePanel.setLayout(new FlowLayout());
    // JLabel vehicleNameLabel = new JLabel("Vehicle Name");
    // JTextField vehicleNameTxt = new JTextField(21);
    // vehicleNamePanel.add(vehicleNameLabel);
    // vehicleNamePanel.add(vehicleNameTxt);
    // panel.add(vehicleNamePanel);

    // Start datetime Input Panel
    JPanel vehiclePanel1 = new JPanel();
    vehiclePanel1.setLayout(new FlowLayout());

    JPanel vehicleStartPanel1 = new JPanel();
    vehicleStartPanel1.setLayout(new FlowLayout());
    JLabel startLabel1 = new JLabel("Start");
    JTextField startDate1 = new JTextField("Date", 10);
    startDate1.setForeground(Color.GRAY);
    setTextboxPlaceholder(startDate1, "Date");
    JTextField startTime1 = new JTextField("Time", 10);
    startTime1.setForeground(Color.GRAY);
    setTextboxPlaceholder(startTime1, "Time");
    vehicleStartPanel1.add(startLabel1);
    vehicleStartPanel1.add(startDate1);
    vehicleStartPanel1.add(startTime1);

    // End datetime Input Panel
    JPanel vehicleEndPanel1 = new JPanel();
    vehicleEndPanel1.setLayout(new FlowLayout());
    JLabel endLabel1 = new JLabel("End");
    JTextField endDate1 = new JTextField("Date", 10);
    endDate1.setForeground(Color.GRAY);
    setTextboxPlaceholder(endDate1, "Date");
    JTextField endTime1 = new JTextField("Time", 10);
    endTime1.setForeground(Color.GRAY);
    setTextboxPlaceholder(endTime1, "Time");
    vehicleEndPanel1.add(endLabel1);
    vehicleEndPanel1.add(endDate1);
    vehicleEndPanel1.add(endTime1);

    JPanel vehicleButtonPanel1 = new JPanel();
    JButton saveBtn1 = new JButton("save");
    JButton deleteBtn1 = new JButton("delete");

    vehicleButtonPanel1.setLayout(new FlowLayout());
    vehicleButtonPanel1.add(saveBtn1);
    vehicleButtonPanel1.add(deleteBtn1);
    vehicleButtonPanel1.setPreferredSize(new Dimension(300, 30));

    vehiclePanel1.add(vehicleStartPanel1);
    vehiclePanel1.add(vehicleEndPanel1);
    vehiclePanel1.add(vehicleButtonPanel1);

    vehiclePanel1.setBorder(BorderFactory.createTitledBorder("Vehicle 1"));
    vehiclePanel1.setPreferredSize(new Dimension(350, 160));
    panel.add(vehiclePanel1);


    JPanel vehiclePanel2 = new JPanel();
    vehiclePanel2.setLayout(new FlowLayout());

    JPanel vehicleStartPanel2 = new JPanel();
    vehicleStartPanel2.setLayout(new FlowLayout());
    JLabel startLabel2 = new JLabel("Start");
    JTextField startDate2 = new JTextField("Date", 10);
    startDate2.setForeground(Color.GRAY);
    setTextboxPlaceholder(startDate2, "Date");
    JTextField startTime2 = new JTextField("Time", 10);
    startTime2.setForeground(Color.GRAY);
    setTextboxPlaceholder(startTime2, "Time");
    vehicleStartPanel2.add(startLabel2);
    vehicleStartPanel2.add(startDate2);
    vehicleStartPanel2.add(startTime2);

    // End datetime Input Panel
    JPanel vehicleEndPanel2 = new JPanel();
    vehicleEndPanel2.setLayout(new FlowLayout());
    JLabel endLabel2 = new JLabel("End");
    JTextField endDate2 = new JTextField("Date", 10);
    endDate2.setForeground(Color.GRAY);
    setTextboxPlaceholder(endDate2, "Date");
    JTextField endTime2 = new JTextField("Time", 10);
    endTime2.setForeground(Color.GRAY);
    setTextboxPlaceholder(endTime2, "Time");
    vehicleEndPanel2.add(endLabel2);
    vehicleEndPanel2.add(endDate2);
    vehicleEndPanel2.add(endTime2);

    JPanel vehicleButtonPanel2 = new JPanel();
    JButton saveBtn2 = new JButton("save");
    JButton deleteBtn2 = new JButton("delete");

    vehicleButtonPanel2.setLayout(new FlowLayout());
    vehicleButtonPanel2.add(saveBtn2);
    vehicleButtonPanel2.add(deleteBtn2);
    vehicleButtonPanel2.setPreferredSize(new Dimension(300, 30));

    vehiclePanel2.add(vehicleStartPanel2);
    vehiclePanel2.add(vehicleEndPanel2);
    vehiclePanel2.add(vehicleButtonPanel2);

    vehiclePanel2.setBorder(BorderFactory.createTitledBorder("Vehicle 2"));
    vehiclePanel2.setPreferredSize(new Dimension(350, 160));
    panel.add(vehiclePanel2);

    frame.add(panel);
  }

  private static void setTextboxPlaceholder(JTextField textbox, String placeholder) {
    textbox.addFocusListener(new FocusListener() {
      @Override
      public void focusGained(FocusEvent e) {
        if (textbox.getText().equals(placeholder)) {
          textbox.setText("");
          textbox.setForeground(Color.BLACK);
        }
      }
      @Override
      public void focusLost(FocusEvent e) {
        if (textbox.getText().isEmpty()) {
          textbox.setForeground(Color.GRAY);
          textbox.setText(placeholder);
        }
      }
    });
  }
}
